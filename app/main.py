import logging

from flask import Flask, request, jsonify
from flask_redis import FlaskRedis

from user import is_email_ok
import db

logging.basicConfig(level=logging.INFO)

app = Flask(__name__)
app.config["REDIS_URL"] = "redis://@db:6379/0"
redis_client = FlaskRedis(app)


@app.before_first_request
def _seed_db():
    db.seed(redis_client)


@app.route("/request", methods=['POST', 'GET'])
def request_():
    if request.method == "POST":
        email = request.form['email']
        title = request.form['title']
        if not is_email_ok(email):
            return "bad email", 400
        if not db.has_title(redis_client, title):
            return "", 204
        result = db.add_new_user_request(redis_client, email, title)
        return jsonify(result)
    elif request.method== "GET":
        res = db.get_all_requests(redis_client)
        return jsonify(res)


@app.route("/request/<request_id>", methods=['GET', 'DELETE'])
def single_request(request_id):
    if request.method == "GET":
        req = db.get_user_request(redis_client, request_id)
        if req:
            return jsonify(req)
        return "", 204
    elif request.method=="DELETE":
        db.delete_user_request(redis_client, request_id)
        return "", 204


