import string

def is_email_ok(email):
    # TODO: more could be done here, maybe just use https://pypi.org/project/email-validator/
    if any(char in string.whitespace for char in email):
        return False
    if email.count("@")!=1:
        return False
    domain = email.split("@")[1]
    if domain.count(".") == 0:
        return False
    return True
    
    