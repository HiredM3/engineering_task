import logging
from datetime import datetime
from uuid import uuid4
import json

BOOK_SET = "TITLES"
REQUESTS = "REQUESTS"

def seed(client):
    logging.info("Seeding db")
    with open("booklist.txt", 'r') as fp:
        for line in fp:
            client.sadd(BOOK_SET, line.strip())


def has_title(client, title):
    return client.sismember(BOOK_SET, title)


def add_new_user_request(client, email, title):
    id_ = str(uuid4())
    req = {
        "email":email,
        "title":title,
        # TODO: add timezone to this
        "timestamp":datetime.now().timestamp()
        }
    jreq = json.dumps(req)
    client.hmset(REQUESTS, {id_: jreq})
    req["id"] = id_
    return req


def get_user_request(client, id_):
    res = client.hget(REQUESTS, id_)
    if res:
        res = json.loads(res)
        res['id'] = id_
        return res


def get_all_requests(client):
    raw_result = client.hgetall(REQUESTS)
    result = []
    for id_ in raw_result:
        result.append(json.loads(raw_result[id_]))
        result[-1]['id'] = str(id_)
    logging.info(result)
    return result

def delete_user_request(client, id_):
    client.hdel(REQUESTS, id_)
