# liquidnet library
## To Run
First bring up the database with :

`docker-compose up -d db`

or

`make db-up&`

if you want to use the make file

Once the redis database is up and running you can then start the main flask application with:

`docker-compose up library`

or 

`make library-up`

The flask api can now be access on your localhost on port 5000.

If you have curl installed you can test it with:

`curl -d 'email=joblog@gmail.com&title=Ion' http://localhost:5000/request`

There is a full functional test suite in its own container that can be run with simply 

`docker-compose up tester`

or

`make run-tests`

I also wrote a very simple bash script called test.sh that will create the database, create the flask app, run the tests and then pull everything down.

