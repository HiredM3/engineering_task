import unittest

import requests

BASE_URL = "http://library"

class TestLibraryREST(unittest.TestCase):

    def test_request_post_happy(self):
        """
        Test request post
        """        
        req_data = {"title":"The Republic", "email":"foobar@gmail.com"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        self.assertEqual(result.status_code, 200)
        res = result.json()
        self.assertEqual(res['email'], req_data['email'])
        self.assertEqual(res['title'], req_data['title'])
        self.assertEqual(sorted(res.keys()), 
                         sorted(['email', 'title', 'id', 'timestamp']))

    def test_request_post_title_not_found(self):
        """
        Test request post but the title should not exist in the db
        """        
        req_data = {"title":"Book title does not exist", "email":"foobar@gmail.com"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        self.assertEqual(result.status_code, 204)

    def test_request_post_bad_email(self):
        """
        Test request post for an invalid email address
        """        
        req_data = {"title":"The Republic", "email":"jojojojojoATgoogleorsomething"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        self.assertEqual(result.status_code, 400)

    def test_request_post_no_email(self):
        """
        Test request post for no email provided
        """        
        req_data = {"title":"The Republic"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        self.assertEqual(result.status_code, 400)

    def test_request_post_no_title(self):
        """
        Test request post for no title provided
        """        
        req_data = {"email":"foobar@gmail.com"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        self.assertEqual(result.status_code, 400)
    
    def test_create_and_get_single_request_happy(self):
        req_data = {"title":"The Republic", "email":"foobar1@gmail.com"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        init_resp = result.json()
        result = requests.get(BASE_URL + "/request/%s" % init_resp['id'])
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.json(), init_resp)

    def test_get_single_request_not_exist(self):
        id_ = "64c4fbb5-9058-4322-837b-3207831b2000"
        result = requests.get(BASE_URL + "/request/%s" % id_)
        self.assertEqual(result.status_code, 204)
        self.assertEqual(result.text, "")

    def test_create_and_get_multiple_request_happy(self):
        test_email = "foobar3@gmail.com"
        req_data = [{"title":"The Republic", "email":"foobar2@gmail.com"},
                    {"title":"Treasure Island", "email":test_email}]
        for req in req_data:
            requests.post(BASE_URL + "/request", data=req)
        result = requests.get(BASE_URL + "/request")
        self.assertEqual(result.status_code, 200)
        all_results = result.json()
        self.assertIn(test_email, [d['email'] for d in all_results])
        for req in all_results:
            self.assertEqual(sorted(req.keys()), 
                             sorted(['email', 'title', 'id', 'timestamp']))

    def test_delete_request_happy(self):
        req_data = {"title":"A Tale of Two Cities", "email":"foobar4@gmail.com"}
        result = requests.post(BASE_URL + "/request", data=req_data)
        result = result.json()
        del_status = requests.delete(BASE_URL + "/request/%s" % result['id'])
        self.assertNotEqual(del_status.status_code//100, 5)
        get_req = requests.get(BASE_URL + "/request/%s" % result['id'])
        self.assertEqual(get_req.status_code, 204)
        self.assertEqual(get_req.text, "")
    

if __name__ == '__main__':
    unittest.main()