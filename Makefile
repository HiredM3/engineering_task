run-tests: 
	docker-compose up --remove-orphans --build tester

library-up:
	docker-compose up --remove-orphans --build library

db-up:
	docker-compose up --remove-orphans db

down:
	docker-compose down

